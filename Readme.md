<h1>Inventi Weather</h1>


Functionality:
When app is started, splash screen with inventi logo and hand is shown for 3 seconds. After that HomeActivity with HomeFragment is shown. User can type in his desired location in textbox and if internet connection is available current weather and weather forecast for 7 days is shown to him. All data received from API is stored in persistent storage so if user opens the app again, last visible data are shown again.This app shows weather in last location that user typed in once per hour in notification..

Implementation info:
App is implemented in MVVC architecture using Data Binding. I choosed MVVC because I wanted to try to use Data Binding and MVVC seemed best for that case.

About task assignment:
I think I covered almost everything that was in task assignment. UI of the app could look better if I would have spend more time on that and there is many other features that could be there if I would spend more time on task (i.e. error handling). I didn't implement left drawer menu(which was in the task assignment), because I couldn't think of anything that could be in it.


