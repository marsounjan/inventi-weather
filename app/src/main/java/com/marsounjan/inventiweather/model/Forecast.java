package com.marsounjan.inventiweather.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class Forecast extends RealmObject {

    public RealmList<ForecastItem> list;

    public boolean isConsistent(){
        return list != null;
    }

}
