package com.marsounjan.inventiweather.model;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import io.realm.Realm;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {
        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //clear all objects from Book.class
    public void setCurrentWeather(CurrentWeather currentWeather) {
        realm.beginTransaction();
        realm.delete(CurrentWeather.class);
        if(currentWeather != null){
            realm.copyToRealm(currentWeather);
        }
        realm.commitTransaction();
    }

    //find all objects in the Book.class
    public CurrentWeather getCurrentWeather() {
        return realm.where(CurrentWeather.class).findFirst();
    }

    //clear all objects from Book.class
    public void setForecast(Forecast forecast) {
        realm.beginTransaction();
        realm.delete(Forecast.class);
        if(forecast != null) {
            realm.copyToRealm(forecast);
        }
        realm.commitTransaction();
    }

    //find all objects in the Book.class
    public Forecast getForecast() {
        return realm.where(Forecast.class).findFirst();
    }
}
