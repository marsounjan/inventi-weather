package com.marsounjan.inventiweather.model;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public interface OpenWeatherMapService {

    @GET("data/2.5/weather")
    Observable<CurrentWeather> currentWeatherForCity(@Query("APIKEY") String apiKey, @Query("units") String units,  @Query("q") String cityName);

    @GET("data/2.5/forecast/daily")
    Observable<Forecast> getForecast16ForCity(@Query("APIKEY") String apiKey, @Query("units") String units, @Query("q") String cityName, @Query("cnt") int days);

    class Factory {
        public static OpenWeatherMapService create() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://api.openweathermap.org/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(OpenWeatherMapService.class);
        }
    }

}
