package com.marsounjan.inventiweather.model;

import java.util.Calendar;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class ForecastItem extends RealmObject {

    public Long dt;
    public Temperatures temp;
    public RealmList<Weather> weather;

    public Calendar getForecastItemTimestamp(){
        //openweathermap returns timestamp in seconds, so it's necessary to multiply it by 1000
        if(dt != null){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date(dt * 1000));
            return calendar;
        }
        return null;
    }

}
