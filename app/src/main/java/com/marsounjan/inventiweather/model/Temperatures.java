package com.marsounjan.inventiweather.model;

import io.realm.RealmObject;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class Temperatures extends RealmObject {
    public Double day;
    public Double min;
    public Double max;
    public Double night;
    public Double eve;
    public Double morn;
}
