package com.marsounjan.inventiweather.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class CurrentWeather extends RealmObject {

    public String name;
    public RealmList<Weather> weather;
    public Main main;

    public boolean isConsistent(){
        if(name == null || weather == null || main == null){
            return false;
        } else {
            return true;
        }
    }

    public String getLocationName() {
        return name;
    }

    public String getDescription(){
        if(weather != null && weather.size() > 0){
            return weather.get(0).description;
        } else {
            return null;
        }
    }

    public String getIconUrl(){
        if(weather != null && weather.size() > 0 && weather.get(0).icon != null){
            return "http://openweathermap.org/img/w/" + weather.get(0).icon + ".png";
        } else {
            return null;
        }
    }

}
