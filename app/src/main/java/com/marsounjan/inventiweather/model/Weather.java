package com.marsounjan.inventiweather.model;

import com.marsounjan.inventiweather.R;

import io.realm.RealmObject;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class Weather extends RealmObject {

    public Integer id;
    public String main;
    public String description;
    public String icon;

    public int getSmallNotificationIconResource(){
        if(id != null){
            if(id >= 200 && id < 600){
                return R.drawable.umbrella;
            } else if(id > 600 && id < 700){
                return R.drawable.snowflake;
            } else if(id == 800){
                return R.drawable.sunny;
            } else if(id >= 801 && id <= 802){
                return R.drawable.clouds_and_sun;
            } else if(id > 802 && id < 900){
                return R.drawable.cloud;
            }
        }
        return R.mipmap.ic_launcher;
    }


}
