package com.marsounjan.inventiweather.model;

import io.realm.RealmObject;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class Main extends RealmObject {

    public Double temp;
    public Double temp_min;
    public Double temp_max;

}
