package com.marsounjan.inventiweather;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;

import com.marsounjan.inventiweather.model.OpenWeatherMapService;
import com.marsounjan.inventiweather.model.RealmController;

import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class InventiWeatherApplication extends Application {

    private OpenWeatherMapService openWeatherMapService;
    private Scheduler defaultSubscribeScheduler;

    @Override
    public void onCreate() {
        super.onCreate();

        setupRealm();
        setupWeatherNotificationAlarmIfNeccessary();
    }

    private void setupRealm(){
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        RealmController.with(this);
    }

    private void setupWeatherNotificationAlarmIfNeccessary(){
        Intent intent = new Intent(this, WeatherReceiver.class);
        intent.setAction(WeatherReceiver.ACTION);
        boolean alarmUp = (PendingIntent.getBroadcast(this, 0,
                intent,
                PendingIntent.FLAG_NO_CREATE) != null);
        if (!alarmUp)
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.add(Calendar.HOUR, 1);
            AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), DateUtils.HOUR_IN_MILLIS, pendingIntent);
        }
    }

    public static InventiWeatherApplication get(Context context) {
        return (InventiWeatherApplication) context.getApplicationContext();
    }

    public OpenWeatherMapService getOpenWeatherMapService() {
        if (openWeatherMapService == null) {
            openWeatherMapService = OpenWeatherMapService.Factory.create();
        }
        return openWeatherMapService;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }
}
