package com.marsounjan.inventiweather.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.marsounjan.inventiweather.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class SplashActivity extends AppCompatActivity {

    private final int APP_START_DELAY = 3000;

    @BindView(R.id.img_logo_top) ImageView img_logoTop;
    @BindView(R.id.img_logo_bottom) ImageView img_logoBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        Picasso.with(this).load(R.drawable.ruka).fit().centerInside().into(img_logoTop);
        Picasso.with(this).load(R.drawable.logo).fit().centerInside().into(img_logoBottom);

        startAppDelayed();

    }

    private void startAppDelayed() {
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                SplashActivity.this.startActivity(intent);
                SplashActivity.this.finish();
            }
        }, APP_START_DELAY);
    }

}
