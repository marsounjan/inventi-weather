package com.marsounjan.inventiweather.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.marsounjan.inventiweather.R;
import com.marsounjan.inventiweather.view.fragment.HomeFragment;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class HomeActivity extends AppCompatActivity {

    private final String FRAGMENT_TAG_HOME = "homeFragment";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_main);

        FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.Fragment fragment = fm.findFragmentByTag(FRAGMENT_TAG_HOME);
        if(fragment == null){
            fragment = new HomeFragment();
            fm.beginTransaction().replace(R.id.main_container,fragment, FRAGMENT_TAG_HOME).commit();
        }
    }
}
