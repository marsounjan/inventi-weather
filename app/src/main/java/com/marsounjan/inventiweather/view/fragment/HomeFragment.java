package com.marsounjan.inventiweather.view.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.marsounjan.inventiweather.ForecastAdapter;
import com.marsounjan.inventiweather.R;
import com.marsounjan.inventiweather.databinding.FragmentHomeBinding;
import com.marsounjan.inventiweather.model.ForecastItem;
import com.marsounjan.inventiweather.viewmodel.HomeViewModel;

import java.util.List;


/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class HomeFragment extends Fragment implements HomeViewModel.DataListener {

    private FragmentHomeBinding binding;
    private HomeViewModel homeViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        View mainView = binding.getRoot();

        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupRecyclerView(binding.recyclerView);

        homeViewModel = new HomeViewModel(getActivity(), this);
        binding.setViewModel(homeViewModel);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        homeViewModel.destroy();
    }

    @Override
    public void onForecastChanged(List<ForecastItem> forecast) {
        ForecastAdapter adapter =
                (ForecastAdapter) binding.recyclerView.getAdapter();
        adapter.setForecast(forecast);
        adapter.notifyDataSetChanged();
        hideSoftKeyboard();
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        ForecastAdapter adapter = new ForecastAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(binding.edtCityName.getWindowToken(), 0);
    }

}
