package com.marsounjan.inventiweather;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.marsounjan.inventiweather.model.CurrentWeather;
import com.marsounjan.inventiweather.model.OpenWeatherMapService;
import com.marsounjan.inventiweather.model.RealmController;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class WeatherNotificationService extends Service {

    private NotificationManager notificationManager;
    private Subscription subscription;
    private CurrentWeather currentWeather;
    private Target iconLoadTarget;

    public WeatherNotificationService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        subscription = null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        InventiWeatherApplication application = InventiWeatherApplication.get(this);
        OpenWeatherMapService openWeatherMapService = application.getOpenWeatherMapService();
        RealmController realmController = RealmController.getInstance();
        //check current weather only if user has setted up some location
        if(realmController.getCurrentWeather() != null && realmController.getCurrentWeather().getLocationName() != null){
            subscription = openWeatherMapService.currentWeatherForCity(this.getString(R.string.openweathermap_api_key),"metric",realmController.getCurrentWeather().getLocationName())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(application.defaultSubscribeScheduler())
                    .subscribe(new Subscriber<CurrentWeather>() {
                        @Override
                        public void onCompleted() {
                            if(currentWeather != null && currentWeather.getIconUrl() != null){
                                loadCurrentWeatherIcon(currentWeather.getIconUrl());
                            } else {
                                WeatherNotificationService.this.stopSelf();
                            }

                        }

                        @Override
                        public void onError(Throwable error) {
                            WeatherNotificationService.this.stopSelf();
                        }

                        @Override
                        public void onNext(CurrentWeather weather) {
                            currentWeather = weather;
                        }
                    });
        } else {
            stopSelf();
        }

        return Service.START_NOT_STICKY;
    }

    public void loadCurrentWeatherIcon(String url) {

        if (iconLoadTarget == null) iconLoadTarget = new Target() {

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                // do nothing
            }

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                // do something with the Bitmap
                handleLoadedBitmap(bitmap);
                WeatherNotificationService.this.stopSelf();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                WeatherNotificationService.this.stopSelf();
            }

        };

        Picasso.with(this).load(url).into(iconLoadTarget);
    }

    public void handleLoadedBitmap(Bitmap b) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(WeatherNotificationService.this)
                        .setSmallIcon(currentWeather.weather.get(0).getSmallNotificationIconResource())
                        .setContentTitle(String.valueOf(currentWeather.main.temp))
                        .setContentText(currentWeather.weather.get(0).description)
                        .setLargeIcon(b);

        notificationManager.notify(1,mBuilder.build());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
