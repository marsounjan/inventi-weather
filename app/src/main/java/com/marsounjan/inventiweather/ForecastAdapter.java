package com.marsounjan.inventiweather;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.marsounjan.inventiweather.databinding.ListItemForecastBinding;
import com.marsounjan.inventiweather.model.ForecastItem;
import com.marsounjan.inventiweather.viewmodel.ForecastItemViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {

    private List<ForecastItem> forecastItems;

    public ForecastAdapter() {
        this.forecastItems = Collections.emptyList();
    }

    public ForecastAdapter(List<ForecastItem> forecast) {
        this.forecastItems = forecast;
    }

    public void setForecast(List<ForecastItem> forecast) {
        this.forecastItems = forecast;
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemForecastBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_forecast,
                parent,
                false);
        return new ForecastViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ForecastViewHolder holder, int position) {
        holder.bindForecastItem(forecastItems.get(position));
    }

    @Override
    public int getItemCount() {
        if(forecastItems != null){
            return forecastItems.size();
        } else {
            return 0;
        }
    }

    public static class ForecastViewHolder extends RecyclerView.ViewHolder {
        final ListItemForecastBinding binding;

        public ForecastViewHolder(ListItemForecastBinding binding) {
            super(binding.cardView);
            this.binding = binding;
        }

        void bindForecastItem(ForecastItem repository) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ForecastItemViewModel(itemView.getContext(), repository));
            } else {
                binding.getViewModel().setRepository(repository);
            }
        }
    }
}
