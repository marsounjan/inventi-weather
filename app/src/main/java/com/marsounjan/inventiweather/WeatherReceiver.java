package com.marsounjan.inventiweather;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class WeatherReceiver extends BroadcastReceiver {

    public static final String ACTION = "checkCurrentWeatherAndShowNotification";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(ACTION)){
            context.startService(new Intent(context, WeatherNotificationService.class));
        }
    }
}
