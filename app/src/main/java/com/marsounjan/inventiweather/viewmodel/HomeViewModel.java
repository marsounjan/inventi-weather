package com.marsounjan.inventiweather.viewmodel;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.marsounjan.inventiweather.InventiWeatherApplication;
import com.marsounjan.inventiweather.R;
import com.marsounjan.inventiweather.model.CurrentWeather;
import com.marsounjan.inventiweather.model.Forecast;
import com.marsounjan.inventiweather.model.ForecastItem;
import com.marsounjan.inventiweather.model.OpenWeatherMapService;
import com.marsounjan.inventiweather.model.RealmController;
import com.squareup.picasso.Picasso;

import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class HomeViewModel implements ViewModel{

    public ObservableInt currentWeatherContainerVisibility;
    public ObservableInt progressVisibility;
    public ObservableField<String> imageUrl;
    private Context context;
    private Subscription subscription;
    private Subscription forecastSubscription;

    public ObservableField<CurrentWeather> weather;
    private DataListener dataListener;
    private String editTextUsernameValue;

    public HomeViewModel(Context context, DataListener dataListener) {
        this.context = context;
        this.dataListener = dataListener;

        currentWeatherContainerVisibility = new ObservableInt(View.INVISIBLE);
        progressVisibility = new ObservableInt(View.INVISIBLE);

        imageUrl = new ObservableField<>();
        weather = new ObservableField<>();

        CurrentWeather currentWeather = RealmController.getInstance().getCurrentWeather();
        if(currentWeather != null && currentWeather.isValid()){
            weather.set(currentWeather);
            progressVisibility.set(View.INVISIBLE);
            currentWeatherContainerVisibility.set(View.VISIBLE);
        }

        Forecast forecast = RealmController.getInstance().getForecast();
        if(forecast != null &&  forecast.isValid() && dataListener != null){
            dataListener.onForecastChanged(forecast.list);
        }

    }

    @Override
    public void destroy() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        if (forecastSubscription != null && !forecastSubscription.isUnsubscribed()) forecastSubscription.unsubscribe();
        subscription = null;
        forecastSubscription = null;
        context = null;
        dataListener = null;
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String url) {
        Picasso.with(view.getContext()).load(url).into(view);
    }

    @BindingAdapter({"temperature"})
    public static void setTemp(TextView view, Double temp) {
        view.setText(String.valueOf(temp));
    }

    @BindingAdapter({"tempMin", "tempMax"})
    public static void setMinMax(TextView view, Double tempMin, Double tempMax) {
        view.setText(String.format(view.getContext().getString(R.string.forecast_item_temp_min_max), tempMin, tempMax));
    }

    @BindingAdapter({"bind:currentWeatherTitle"})
    public static void setCurrentWeatherTitle(TextView view, String cityName) {
        view.setText(String.format(view.getResources().getString(R.string.current_weather_title), cityName));
    }

    public void setDataListener(DataListener dataListener) {
        this.dataListener = dataListener;
    }



    public boolean onSearchAction(TextView view, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String username = view.getText().toString();
            if (username.length() > 0){
                loadCurrentWeather(username);
                loadWeatherForecast(username);
            }
            return true;
        }
        return false;
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        editTextUsernameValue = String.valueOf(s);
    }

    public void onClickSearch(View view) {
        loadCurrentWeather(editTextUsernameValue);
        loadWeatherForecast(editTextUsernameValue);
    }

    private void loadCurrentWeather(final String cityName) {

        progressVisibility.set(View.VISIBLE);
        currentWeatherContainerVisibility.set(View.INVISIBLE);
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        InventiWeatherApplication application = InventiWeatherApplication.get(context);
        OpenWeatherMapService openWeatherMapService = application.getOpenWeatherMapService();
        subscription = openWeatherMapService.currentWeatherForCity(context.getString(R.string.openweathermap_api_key),"metric",cityName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<CurrentWeather>() {
                    @Override
                    public void onCompleted() {
                        progressVisibility.set(View.INVISIBLE);
                        //if there is no weather response, set current weather layout invisible
                        if(weather.get() != null){
                            currentWeatherContainerVisibility.set(View.VISIBLE);
                        } else {
                            currentWeatherContainerVisibility.set(View.INVISIBLE);
                        }
                        RealmController.getInstance().setCurrentWeather(weather.get());
                    }

                    @Override
                    public void onError(Throwable error) {
                        Log.e(HomeViewModel.class.getName(), "Error loading current weather ", error);
                    }

                    @Override
                    public void onNext(CurrentWeather weather) {
                        //check for consistency here
                        if(weather != null && weather.isConsistent()){
                            HomeViewModel.this.weather.set(weather);
                        } else {
                            HomeViewModel.this.weather.set(null);
                        }

                    }
                });
    }

    private void loadWeatherForecast(final String cityName) {

        if (forecastSubscription != null && !forecastSubscription.isUnsubscribed()) forecastSubscription.unsubscribe();
        InventiWeatherApplication application = InventiWeatherApplication.get(context);
        OpenWeatherMapService openWeatherMapService = application.getOpenWeatherMapService();
        forecastSubscription = openWeatherMapService.getForecast16ForCity(context.getString(R.string.openweathermap_api_key),"metric",cityName, 7)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<Forecast>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable error) {
                        Log.e(HomeViewModel.class.getName(), "Error loading forecast", error);
                    }

                    @Override
                    public void onNext(Forecast forecast) {
                        if (dataListener != null) dataListener.onForecastChanged(forecast.list);
                        RealmController.getInstance().setForecast(forecast);
                    }
                });
    }

    public interface DataListener {
        void onForecastChanged(List<ForecastItem> forecastItemList);
    }

}
