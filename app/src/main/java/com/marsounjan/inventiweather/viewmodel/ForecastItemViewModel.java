package com.marsounjan.inventiweather.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.text.format.DateUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.marsounjan.inventiweather.R;
import com.marsounjan.inventiweather.model.ForecastItem;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public class ForecastItemViewModel extends BaseObservable implements ViewModel {

    private ForecastItem forecastItem;
    public ObservableField<String> imageUrl;
    private Context context;

    public ForecastItemViewModel(Context context, ForecastItem forecastItem) {
        this.forecastItem = forecastItem;
        this.context = context;

        imageUrl = new ObservableField<>();
        imageUrl.set("http://openweathermap.org/img/w/" + forecastItem.weather.get(0).icon + ".png");
    }

    @BindingAdapter({"temp"})
    public static void setTemp(TextView view, Double temp) {
        view.setText(String.format(view.getContext().getString(R.string.forecast_item_temp), temp));
    }

    @BindingAdapter({"tempMin", "tempMax"})
    public static void setMinMax(TextView view, Double tempMin, Double tempMax) {
        view.setText(String.format(view.getContext().getString(R.string.forecast_item_temp_min_max), tempMin, tempMax));
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String url) {
        Picasso.with(view.getContext()).load(url).into(view);
    }

    public Double getTemp() {
        return forecastItem.temp.day;
    }

    public Double getTempMin() {
        return forecastItem.temp.min;
    }

    public Double getTempMax() {
        return forecastItem.temp.max;
    }

    public String getForecastDate() {
        return DateUtils.formatDateTime(
                context,
                forecastItem.getForecastItemTimestamp().getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY
        );
    }

    public String getWeatherDescription() {
        return forecastItem.weather.get(0).description;
    }

    public void setRepository(ForecastItem forecastItem) {
        this.forecastItem = forecastItem;
        notifyChange();
    }

    @Override
    public void destroy() {
        //In this case destroy doesn't need to do anything because there is not async calls
    }
}
