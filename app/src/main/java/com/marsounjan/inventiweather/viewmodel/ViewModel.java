package com.marsounjan.inventiweather.viewmodel;

/**
 * Created by Jan Maršoun (marsounjan@gmail.com) on 05.09.16.
 */
public interface ViewModel {
    void destroy();
}
